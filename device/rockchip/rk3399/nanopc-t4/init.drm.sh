#! /vendor/bin/sh

# work-around for rockchip hwcomposer

drm_base=/sys/class/drm
hdmi_status=$drm_base/card0-HDMI-A-1/status

if ! grep "^connected" $drm_base/card0-*/status >/dev/null; then
    if [ -f $hdmi_status ]; then
        echo on > $hdmi_status
        sleep 2
        echo detect > $hdmi_status
    fi
fi

echo "DRM setup:  Completed" > /dev/kmsg
